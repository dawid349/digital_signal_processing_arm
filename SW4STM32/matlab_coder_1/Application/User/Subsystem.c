/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: Subsystem.c
 *
 * Code generated for Simulink model 'Subsystem'.
 *
 * Model version                  : 1.0
 * Simulink Coder version         : 9.0 (R2018b) 24-May-2018
 * C/C++ source code generated on : Wed Dec 25 13:51:19 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Subsystem.h"
#include "Subsystem_private.h"

/* Block signals (default storage) */
B_Subsystem_T Subsystem_B;

/* Block states (default storage) */
DW_Subsystem_T Subsystem_DW;

/* External inputs (root inport signals with default storage) */
ExtU_Subsystem_T Subsystem_U;

/* External outputs (root outports fed by signals with default storage) */
ExtY_Subsystem_T Subsystem_Y;

/* Real-time model */
RT_MODEL_Subsystem_T Subsystem_M_;
RT_MODEL_Subsystem_T *const Subsystem_M = &Subsystem_M_;

/* Model step function */
void Subsystem_step(void)
{
  boolean_T rEQ0;
  real32_T q;
  real32_T rtb_y;
  real32_T rtb_y_k;
  int32_T i;

  /* FromWorkspace: '<S1>/From Workspace1' */
  {
    real_T *pDataValues = (real_T *) Subsystem_DW.FromWorkspace1_PWORK.DataPtr;

    {
      int_T elIdx;
      for (elIdx = 0; elIdx < 255; ++elIdx) {
        (&Subsystem_B.FromWorkspace1[0])[elIdx] = pDataValues[0];
        pDataValues += 1;
      }
    }
  }

  /* MATLAB Function: '<S1>/delta_sin' incorporates:
   *  Inport: '<Root>/In1'
   *  UnitDelay: '<S2>/Output'
   */
  rtb_y_k = Subsystem_U.In1 * (real32_T)Subsystem_DW.Output_DSTATE;

  /* MATLAB Function: '<S1>/modulo' incorporates:
   *  Constant: '<S1>/Constant1'
   */
  rtb_y = rtb_y_k;
  if ((!rtIsInfF(rtb_y_k)) && (!rtIsNaNF(rtb_y_k)) && ((!rtIsInfF
        (Subsystem_P.len_sine_tab - 1.0F)) && (!rtIsNaNF
        (Subsystem_P.len_sine_tab - 1.0F)))) {
    if (rtb_y_k == 0.0F) {
      rtb_y = (Subsystem_P.len_sine_tab - 1.0F) * 0.0F;
    } else {
      if (Subsystem_P.len_sine_tab - 1.0F != 0.0F) {
        rtb_y = (real32_T)fmod(rtb_y_k, Subsystem_P.len_sine_tab - 1.0F);
        rEQ0 = (rtb_y == 0.0F);
        if ((!rEQ0) && (Subsystem_P.len_sine_tab - 1.0F > (real32_T)floor
                        (Subsystem_P.len_sine_tab - 1.0F))) {
          q = (real32_T)fabs(rtb_y_k / (Subsystem_P.len_sine_tab - 1.0F));
          rEQ0 = ((real32_T)fabs(q - (real32_T)floor(q + 0.5F)) <= 1.1920929E-7F
                  * q);
        }

        if (rEQ0) {
          rtb_y = (Subsystem_P.len_sine_tab - 1.0F) * 0.0F;
        } else {
          if ((Subsystem_P.len_sine_tab - 1.0F < 0.0F) != (rtb_y_k < 0.0F)) {
            rtb_y += Subsystem_P.len_sine_tab - 1.0F;
          }
        }
      }
    }
  } else {
    if (Subsystem_P.len_sine_tab - 1.0F != 0.0F) {
      rtb_y = (rtNaNF);
    }
  }

  /* End of MATLAB Function: '<S1>/modulo' */

  /* Sum: '<S1>/Add' incorporates:
   *  Constant: '<S1>/Constant2'
   */
  rtb_y_k = Subsystem_P.Constant2_Value + rtb_y;

  /* MATLAB Function: '<S1>/table ' */
  for (i = 0; i < 255; i++) {
    Subsystem_B.tab[i] = (real32_T)Subsystem_B.FromWorkspace1[i];
  }

  if (rtb_y_k > 255.0F) {
    rtb_y_k--;
  }

  rtb_y = (real32_T)floor(rtb_y_k);
  q = rtb_y_k - rtb_y;
  if (q != 0.0F) {
    rtb_y = Subsystem_B.tab[(int32_T)rtb_y - 1];
    q = (Subsystem_B.tab[(int32_T)(real32_T)ceil(rtb_y_k) - 1] - rtb_y) * q +
      rtb_y;
  } else {
    q = Subsystem_B.tab[(int32_T)rtb_y_k - 1];
  }

  /* End of MATLAB Function: '<S1>/table ' */

  /* Outport: '<Root>/Out4' incorporates:
   *  Constant: '<S1>/Constant'
   *  Sum: '<S1>/Add1'
   */
  Subsystem_Y.Out4 = Subsystem_P.offset + q;

  /* Sum: '<S6>/FixPt Sum1' incorporates:
   *  Constant: '<S6>/FixPt Constant'
   *  UnitDelay: '<S2>/Output'
   */
  Subsystem_DW.Output_DSTATE = (uint16_T)((uint32_T)Subsystem_DW.Output_DSTATE +
    Subsystem_P.FixPtConstant_Value);

  /* Switch: '<S7>/FixPt Switch' incorporates:
   *  Constant: '<S7>/Constant'
   *  UnitDelay: '<S2>/Output'
   */
  if (Subsystem_DW.Output_DSTATE > Subsystem_P.WrapToZero_Threshold) {
    Subsystem_DW.Output_DSTATE = Subsystem_P.Constant_Value;
  }

  /* End of Switch: '<S7>/FixPt Switch' */

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The resolution of this integer timer is 0.001, which is the step size
   * of the task. Size of "clockTick0" ensures timer will not overflow during the
   * application lifespan selected.
   */
  Subsystem_M->Timing.clockTick0++;
}

/* Model initialize function */
void Subsystem_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)Subsystem_M, 0,
                sizeof(RT_MODEL_Subsystem_T));

  /* states (dwork) */
  (void) memset((void *)&Subsystem_DW, 0,
                sizeof(DW_Subsystem_T));

  /* external inputs */
  Subsystem_U.In1 = 0.0F;

  /* external outputs */
  Subsystem_Y.Out4 = 0.0F;

  /* Start for FromWorkspace: '<S1>/From Workspace1' */
  {
    static real_T pTimeValues0[] = { 0.0 } ;

    static real_T pDataValues0[] = { 25.0, 50.0, 75.0, 100.0, 125.0, 150.0,
      175.0, 200.0, 224.0, 249.0, 273.0, 297.0, 321.0, 345.0, 368.0, 391.0,
      415.0, 437.0, 460.0, 482.0, 504.0, 526.0, 547.0, 568.0, 589.0, 609.0,
      629.0, 649.0, 668.0, 687.0, 705.0, 723.0, 741.0, 758.0, 775.0, 791.0,
      806.0, 822.0, 836.0, 851.0, 864.0, 877.0, 890.0, 902.0, 914.0, 925.0,
      935.0, 945.0, 954.0, 963.0, 971.0, 979.0, 986.0, 992.0, 998.0, 1003.0,
      1008.0, 1012.0, 1015.0, 1018.0, 1020.0, 1022.0, 1023.0, 1023.0, 1023.0,
      1022.0, 1020.0, 1018.0, 1015.0, 1012.0, 1008.0, 1003.0, 998.0, 992.0,
      986.0, 979.0, 971.0, 963.0, 954.0, 945.0, 935.0, 925.0, 914.0, 902.0,
      890.0, 877.0, 864.0, 851.0, 836.0, 822.0, 806.0, 791.0, 775.0, 758.0,
      741.0, 723.0, 705.0, 687.0, 668.0, 649.0, 629.0, 609.0, 589.0, 568.0,
      547.0, 526.0, 504.0, 482.0, 460.0, 437.0, 415.0, 391.0, 368.0, 345.0,
      321.0, 297.0, 273.0, 249.0, 224.0, 200.0, 175.0, 150.0, 125.0, 100.0, 75.0,
      50.0, 25.0, 0.0, -25.0, -50.0, -75.0, -100.0, -125.0, -150.0, -175.0,
      -200.0, -224.0, -249.0, -273.0, -297.0, -321.0, -345.0, -368.0, -391.0,
      -415.0, -437.0, -460.0, -482.0, -504.0, -526.0, -547.0, -568.0, -589.0,
      -609.0, -629.0, -649.0, -668.0, -687.0, -705.0, -723.0, -741.0, -758.0,
      -775.0, -791.0, -806.0, -822.0, -836.0, -851.0, -864.0, -877.0, -890.0,
      -902.0, -914.0, -925.0, -935.0, -945.0, -954.0, -963.0, -971.0, -979.0,
      -986.0, -992.0, -998.0, -1003.0, -1008.0, -1012.0, -1015.0, -1018.0,
      -1020.0, -1022.0, -1023.0, -1023.0, -1023.0, -1022.0, -1020.0, -1018.0,
      -1015.0, -1012.0, -1008.0, -1003.0, -998.0, -992.0, -986.0, -979.0, -971.0,
      -963.0, -954.0, -945.0, -935.0, -925.0, -914.0, -902.0, -890.0, -877.0,
      -864.0, -851.0, -836.0, -822.0, -806.0, -791.0, -775.0, -758.0, -741.0,
      -723.0, -705.0, -687.0, -668.0, -649.0, -629.0, -609.0, -589.0, -568.0,
      -547.0, -526.0, -504.0, -482.0, -460.0, -437.0, -415.0, -391.0, -368.0,
      -345.0, -321.0, -297.0, -273.0, -249.0, -224.0, -200.0, -175.0, -150.0,
      -125.0, -100.0, -75.0, -50.0, -25.0 } ;

    Subsystem_DW.FromWorkspace1_PWORK.TimePtr = (void *) pTimeValues0;
    Subsystem_DW.FromWorkspace1_PWORK.DataPtr = (void *) pDataValues0;
    Subsystem_DW.FromWorkspace1_IWORK.PrevIndex = 0;
  }

  /* InitializeConditions for UnitDelay: '<S2>/Output' */
  Subsystem_DW.Output_DSTATE = Subsystem_P.Output_InitialCondition;
}

/* Model terminate function */
void Subsystem_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
