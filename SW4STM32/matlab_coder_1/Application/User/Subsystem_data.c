/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: Subsystem_data.c
 *
 * Code generated for Simulink model 'Subsystem'.
 *
 * Model version                  : 1.0
 * Simulink Coder version         : 9.0 (R2018b) 24-May-2018
 * C/C++ source code generated on : Wed Dec 25 13:51:19 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Subsystem.h"
#include "Subsystem_private.h"

/* Block parameters (default storage) */
P_Subsystem_T Subsystem_P = {
  /* Variable: len_sine_tab
   * Referenced by: '<S1>/Constant1'
   */
  256.0F,

  /* Variable: offset
   * Referenced by: '<S1>/Constant'
   */
  1024.0F,

  /* Mask Parameter: WrapToZero_Threshold
   * Referenced by: '<S7>/FixPt Switch'
   */
  65535U,

  /* Computed Parameter: Constant2_Value
   * Referenced by: '<S1>/Constant2'
   */
  1.0F,

  /* Computed Parameter: Constant_Value
   * Referenced by: '<S7>/Constant'
   */
  0U,

  /* Computed Parameter: Output_InitialCondition
   * Referenced by: '<S2>/Output'
   */
  0U,

  /* Computed Parameter: FixPtConstant_Value
   * Referenced by: '<S6>/FixPt Constant'
   */
  1U
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
