# digital_signal_processing_ARM

My project for subject "Theory and Digital Signal Processing". Its implements sinewaves signals generator based on STM32F411RET MCU. Main logic of application was create using MATLAB Code Embedded Code Generator feature in MATLAB SIMULINK. Hardware-in-the-loop (HIL) simulation was performed on the project. Look-up table technique was used to generate signals.
