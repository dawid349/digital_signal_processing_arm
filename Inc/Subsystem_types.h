/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: Subsystem_types.h
 *
 * Code generated for Simulink model 'Subsystem'.
 *
 * Model version                  : 1.0
 * Simulink Coder version         : 9.0 (R2018b) 24-May-2018
 * C/C++ source code generated on : Wed Dec 25 13:51:19 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Subsystem_types_h_
#define RTW_HEADER_Subsystem_types_h_
#include "rtwtypes.h"

/* Parameters (default storage) */
typedef struct P_Subsystem_T_ P_Subsystem_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_Subsystem_T RT_MODEL_Subsystem_T;

#endif                                 /* RTW_HEADER_Subsystem_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
